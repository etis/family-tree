myFamily = function () {
    family = new Family()

    family.surname(["Nowak"]);
    family.surname(["Kowalski", "Kowalska"]);

    // Generation -3

    var leonKowalski = family.man({
        name: "Leon", family: "Kowalski"
    });

    var monikaKowalska = family.woman({
        name: "Monika", surname: "Kowalska"
    })

    // Generation -2

    var albertNowak = family.man({
        name: "Albert", family: "Nowak"
    });

    var barbaraNowak = family.woman({
        name: "Barbara", surname: "Nowak"
    })

    var dianaKowalska = family.woman({
        name: "Diana", surname: "Kowalska"
    });

    var erykKowalski = family.man({
        name: "Eryk", family: "Kowalski",
        mother: monikaKowalska, father: leonKowalski
    })

    // Generation -1

    var jakubKowalski = family.man({
        name: "Jakub", family: "Kowalski",
        mother: dianaKowalska, father: erykKowalski
    });

    var feliksNowak = family.man({
        name: "Feliks", family: "Nowak",
        mother: barbaraNowak, father: albertNowak
    });

    var helenaNowak = family.woman({
        name: "Helena", family: "Kowalska", surname: "Nowak",
        mother: dianaKowalska, father: erykKowalski
    });

    // Generation 0

    var ignacyNowak = family.man({
        name: "Ignacy", family: "Nowak",
        mother: helenaNowak, father: feliksNowak
    });

    var celinaNowak = family.woman({
        name: "Celina", family: "Nowak",
        mother: helenaNowak, father: feliksNowak
    });

    var karolinaKowalska = family.woman({
        name: "Karolina", family: "Kowalska",
        father: jakubKowalski
    });

    return family;
};