
function D3FamilyTree(family, center) {
    var tree = new FamilyTree(family);

    var simulation = d3.forceSimulation(tree.nodes)
        .alphaTarget(0.5)
        //.force("link", d3.forceLink(links).distance(100))
        //.force("charge", d3.forceManyBody())
        .force("charge", d3.forceCollide(function (personNode) {
            return personNode.width / 2 + 20;
        }))
        .force("center", forceCenter(Object.values(tree.personNodes), center))
        .force("relations", forceRelations(tree.personNodes))

    this.tree = tree;
    this.simulation = simulation;
}