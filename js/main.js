document.addEventListener("DOMContentLoaded", function(e) {
    new D3SvgFamilyTree(myFamily())
        .render(d3.select("#family-tree"));
});