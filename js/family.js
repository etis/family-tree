function id() {
    function guidGenerator() {
        var S4 = function() {
           return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
        };
        return (S4()+S4()+"-"+S4()+"-"+S4()+"-"+S4()+"-"+S4()+S4()+S4());
    }
    return guidGenerator();
}

function Person(
    person,
    gender
) {
    this.id = id();
    this.name = person.name;
    if (person.surname) {
        this.surname = person.surname;
    } else {
        this.surname = person.family;
    }

    this.gender = gender;
    this.family = person.family;
    this.father = person.father;
    this.mother = person.mother;
    this.relations = {};

    if (this.surname) {
        this.displayName = this.name + " " + this.surname;
    } else {
        this.displayName = this.name;
    }
}

Gender = {
    MAN: "man",
    WOMAN: "woman"
};

function Relation(
    mother,
    father
) {
    this.id = id();
    this.mother = mother;
    this.father = father;
    this.children = [];
};

function Family() {
    var members = {};
    var familyNames = {};
    var relations = {};

    function addRelation(father, mother) {
        var relation = new Relation(mother, father);
        if(father.relations[mother.id]) {
            relation = father.relations[mother.id];
        }
        if(mother.relations[father.id]) {
            relation = mother.relations[father.id];
        }

        father.relations[mother.id] = relation;
        mother.relations[father.id] = relation;
        relations[relation.id] = relation;
        return relation;
    };

    function addPerson(personData, gender) {
        var person = new Person(personData, gender);
        members[person.id] = person;

        if (person.father || person.mother) {
            if (!person.father) {
                person.father = addUnknownParent(Gender.MAN);
            }
            if (!person.mother) {
                person.mother = addUnknownParent(Gender.WOMAN);
            }
            var relation = addRelation(person.father, person.mother);
            relation.children.push(person);
        }

        return person;
    };

    function addUnknownParent(gender) {
        return addPerson({
            name: "???"
        }, gender);
    };

    // set

    this.man = function (man) {
        return addPerson(man, Gender.MAN);
    };

    this.woman = function (woman) {
        return addPerson(woman, Gender.WOMAN);
    };

    this.surname = function (surnames) {
        surnames.forEach(function (surname) {
            familyNames[surname] = surnames;
        });
    };

    // get

    this.members = function () {
        return Object.values(members);
    };

    this.relations = function () {
        return Object.values(relations);
    };
};