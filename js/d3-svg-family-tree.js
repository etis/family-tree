function textSize(text) {
    if (!d3) return;
    var container = d3.select('body').append('svg');
    container.append('text').attr("x", -99999).attr('y', -99999).text(text);
    var size = container.node().getBBox();
    container.remove();
    return { width: size.width, height: size.height };
};

function SvgPath() {
    var path = [];
    var x = 0;
    var y = 0;

    function operation(operation, newX, newY) {
        x = newX; y = newY;
        path.push(operation + x + "," + y);
    }

    this.move = function (point) {
        operation("M", point.x, point.y);
    };

    this.line = function (point) {
        operation("L", point.x, point.y);
    };

    this.elbow = {
        vertical: function (point, flexion) {
            operation("L", x, flexion);
            operation("L", point.x, flexion);
            operation("L", point.x, point.y);
        },
        horisontal: function (point, flexion) {
            operation("L", flexion, y);
            operation("L", flexion, point.y);
            operation("L", point.x, point.y);
        }
    }

    this.get = function () {
        return path.join();
    };
}

function D3SvgFamilyTree(family) {
    this.render = function (div) {
        function width() {
            return div.node().getBoundingClientRect().width;
        }
        function height() {
            return div.node().getBoundingClientRect().height;
        }

        var dynamicCenter = {
            x: 0,
            y: 0
        };

        var d3FamilyTree = new D3FamilyTree(family, {
            x: function() { return width() / 2 + dynamicCenter.x },
            y: function() { return height() / 2 + dynamicCenter.y },
        });

        var svgDoc = div
            .append("svg")
            .attr("width", "100%")
            .attr("height", "100%");

        var container = svgDoc
            .append("rect")
            .attr("class", "background")
            .attr("width", "100%")
            .attr("height", "100%")
            .call(d3.zoom()
                .on('zoom', function(){
                    div.select("svg > g").attr("transform", d3.event.transform);
                })
            );

        var container = svgDoc
            .append("g");

        var nodesData = container.selectAll(".node").data(d3FamilyTree.tree.nodes);

        nodesData
            .enter()
            .filter(function (node) { return node.class === "relation" } )
            .append("g")
            .attr("id", function (node) { return node.index; })
            .attr("class", function (node) { return "node " + node.class; });

        nodesData
            .enter()
            .filter(function (node) { return node.class === "person" } )
            .append("g")
            .attr("id", function (node) { return node.index; })
            .attr("class", function (node) { return "node " + node.class; });

        var rects = container.selectAll(".node.person")
            .append("rect")
            .attr("class", "person-border")
            .call(d3.drag()
                .on("start", function (node) { node.fx = node.x; node.fy = node.y; })
                .on("drag", function (node) { node.fx = d3.event.x; node.fy = d3.event.y; })
                .on("end", function (node) { node.fx = null; node.fy = null; })
            );

        var texts = container.selectAll(".node.person")
            .append("text")
            //.style("font-size", function(d) { return Math.min(2 * d.r, (2 * d.r - 8) / this.getComputedTextLength() * 24) + "px"; })
            .attr("class", "person-name");

        var paths = container.selectAll(".node.relation")
            .append("path")
            .attr("class", "link");

        d3FamilyTree.simulation
            .nodes(d3FamilyTree.tree.nodes)
            .on("tick", function () {
                rects
                    .attr("x", function (node) {
                        return node.x - node.width / 2;
                    })
                    .attr("y", function (node) {
                        return node.y - node.height / 2;
                    })
                    .attr("width", function (node) {
                        return node.width;
                    })
                    .attr("height", function (node) {
                        return node.height;
                    });

                texts
                    .attr("x", function (node) {
                        return node.x + 5 - node.width / 2;
                    })
                    .attr("y", function (node) {
                        return node.y + 15 - node.height / 2;
                    })
                    .text(function (node) {
                        return node.person.displayName;
                    });

                paths
                    .attr("d", function (relation) {
                        var path = new SvgPath();
                        var relationJoint = relation.joint();
                        path.move(relation.fatherNode);
                        path.line(relation.motherNode);
                        relation.childrenNodes.forEach(function (childNode) {
                            path.move(relationJoint);
                            path.elbow.vertical(childNode, relationJoint.y + 50);
                        });
                        return path.get();
                    });
            });
    }
};