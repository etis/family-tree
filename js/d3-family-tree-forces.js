function NodeForce(
    me,
    other
) {
    function acot(x) {
        if (x == 0) return Math.PI / 2;
        return Math.atan(1/x);
    };

    function dimensionForce(
        myValue,
        otherValue
    ) {
        function fallingRising(diff) {
            return function (mod) {
                if (mod === undefined) { mod = 1; }
                return Math.atan(mod * diff);
            };
        };

        function risingFalling(diff) {
            return function (mod) {
                if (mod === undefined) { mod = 1; }
                return acot(mod * diff);
            };
        };

        function flat(diff) {
            return function (value) {
                return diff > 0 ? value : -value;
            };
        };

        return function (change) {
            if (change === undefined) { change = 0; }
            var pullDiff = otherValue + change - myValue;
            var pushDiff = myValue - otherValue + change;
            return {
                pull: {
                    falling: fallingRising(pullDiff),
                    rising: risingFalling(pullDiff),
                    flat: flat(pullDiff)
                },
                push: {
                    falling: risingFalling(pushDiff),
                    rising: fallingRising(pushDiff),
                    flat: flat(pushDiff)
                }
            };
        }
    };

    this.x = dimensionForce(me.x, other.x);
    this.y = dimensionForce(me.y, other.y);
};

function forceRelations(personNodes) {
    function parentGravity(alpha, node, parentNode) {
        var underParentForce = new NodeForce(node, parentNode).y(150).pull.falling(0.01) * 100;
        node.vy += alpha * underParentForce;
        parentNode.vy -= alpha * underParentForce * 0.5;

        var nearParentForce = new NodeForce(node, parentNode).x().pull.falling(0.01) * 10;
        node.vx += alpha * nearParentForce;
        parentNode.vx -= alpha * nearParentForce * 0.5;
    }

    return function (alpha) {
        Object.values(personNodes).forEach(function (node) {

            if (node.person.father) {
                parentGravity(alpha, node, personNodes[node.person.father.id]);
            }
            if (node.person.mother) {
                parentGravity(alpha, node, personNodes[node.person.mother.id]);
            }
            Object.keys(node.person.relations).forEach(function (mateId) {
                var mateNode = personNodes[mateId];
                var mateNodeForce = new NodeForce(node, mateNode);
                node.vy += alpha * mateNodeForce.y().pull.falling(0.01) * 70;
                node.vx += alpha * mateNodeForce.x().pull.falling(0.01) * 20;
            });
//
//            Object.values(personNodes).forEach(function (other) {
//                if (node.person.id !== other.person.id) {
//                    if (Math.abs(node.y - other.y) < (node.height + other.height) / 2) {
//                        var collisionForce = new NodeForce(node, other).x().push.falling(0.07);
//                        node.vx += alpha * collisionForce;
//                        other.vx -= alpha * collisionForce;
//                    }
//                }
//            });

        });
    }
};

function forceCenter(nodes, screenCenter) {
    return function (alpha) {
        var posSum = { x: 0, y: 0 };
        var nodeCount = 0;
        nodes.forEach(function (node) {
            posSum.x += node.x;
            posSum.y += node.y;
            nodeCount++;
        });
        var center = {
            x: posSum.x / nodeCount,
            y: posSum.y / nodeCount
        };
        Object.values(nodes).forEach(function (node) {
            node.vy -= alpha * (center.y - screenCenter.y());
            node.vx -= alpha * (center.x - screenCenter.x());
        });
    }
}