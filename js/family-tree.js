function Link(source, target) {
    this.source = source;
    this.target = target;
};

function RelationNode(
    relation,
    fatherNode,
    motherNode,
    childrenNodes
) {
    this.class = "relation";
    this.relation = relation;

    this.fatherNode = fatherNode;
    this.motherNode = motherNode;
    this.childrenNodes = childrenNodes;

    this.joint = function () {
        return {
            x: (fatherNode.x + motherNode.x) / 2,
            y: (fatherNode.y + motherNode.y) / 2
        };
    };

    this.links = function () {
        var links = [];
        links.push(new Link(fatherNode, this));
        links.push(new Link(motherNode, this));
        childrenNodes.forEach(function (childNode) {
            links.push(new Link(childNode, this));
        });
    };
};

function PersonNode(person, size) {
    this.class = "person";
    this.person = person;

    this.width = size.width;
    this.height = size.height;
};

function FamilyTree(family) {

    var relationNodes = {};
    var personNodes = {};
    var nodes = [];
    var links = [];
    var line = [];

    family.members().forEach(function (person) {
        var node = new PersonNode(person, { width: 100, height: 25 });
        personNodes[person.id] = node;
        nodes.push(node);
    });
    family.relations().forEach(function (relation) {
        var node = new RelationNode(
            relation,
            personNodes[relation.father.id],
            personNodes[relation.mother.id],
            relation.children.map(function (child) { return personNodes[child.id]; })
        );
        relationNodes[relation.id] = node;
        links.push(node.links());
        nodes.push(node);
    });

    this.nodes = nodes;
    this.personNodes = personNodes;
    this.node = function (id) {
        return personNodes[id];
    }
}